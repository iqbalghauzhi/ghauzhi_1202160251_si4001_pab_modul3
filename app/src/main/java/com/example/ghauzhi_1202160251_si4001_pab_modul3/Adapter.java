package com.example.ghauzhi_1202160251_si4001_pab_modul3;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private List<Orang> orangList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nama, pekerjaan;
        public ImageView img;
        public CardView cvItem;

        public MyViewHolder(View view) {
            super(view);
            nama = (TextView) view.findViewById(R.id.nama);
            pekerjaan = (TextView) view.findViewById(R.id.pekerjaan);
            img = view.findViewById(R.id.img);
            cvItem = view.findViewById(R.id.cv_item);
        }
    }


    public Adapter(Context context, List<Orang> orangList) {
        this.context = context;
        this.orangList = orangList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Orang orang= orangList.get(position);
        holder.pekerjaan.setText(orang.job);
        holder.nama.setText(orang.nama);
        if (orang.getFoto() == 0) {
            holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.cowok));
        } else {
            holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.cewek));
        }
        holder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Detail.class);
                intent.putExtra("data", orang);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.orangList.size();
    }
}